package Jar_File;

import java.util.Scanner;

public class MultipleTable {

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int row, colum;
		System.out.println("Enter the row?");
		row = in.nextInt();
		System.out.println("Enter the colum?");
		colum = in.nextInt();

		for (int i = 1; i <= colum; i++) {
			for (int j = 1; j <= row; j++) {
				System.out.print(i * j + "\t");
			}

			System.out.println();
		}
	}

}
